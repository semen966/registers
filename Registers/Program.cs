﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registers
{
    /// <summary>
    /// Расчет значений регистров 0, 1, 2 и 13 (D).
    /// Остальные регистры остаются неизменными для каждого диапазона
    /// </summary>
    class Program
    {
        /// <summary> Частота гетеродина для L1, Мгц </summary>
        const double FG_L1 = 5939.58d;
        /// <summary> Частота гетеродина для L2, Мгц </summary>
        const double FG_L2 = 6287.40d;
        /// <summary> Частота гетеродина для L5, Мгц </summary>
        const double FG_L5 = 6338.55d;
        /// <summary> Частота фазового детектора, МГц</summary>
        const double FD = 10d;
        /// <summary> Шаг перестройки, Гц </summary>
        const int STEP = 500;
        /// <summary> Частотный диапазон </summary>
        enum GNSSRange { L1, L2, L5 };
        /// <summary> Авт. установка ADC clock </summary>
        const int R10 = 0xC0067A;

        static void Main(string[] args)
        {
            //регистры, чьи значения вычисляем
            int r0, r1, r2, rD;
            //частота Допплера
            int fd = 0;
            string str = "";
            GNSSRange range = 0;

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("###########################################");
            Console.Write("# ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Программа расчета регистров синтезатора");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(" #");
            Console.WriteLine("###########################################");
            Console.WriteLine();

            while (true)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Введите диапазон (L1, L2, L5)");

                Console.ForegroundColor = ConsoleColor.Yellow;
                str = Console.ReadLine();

                switch (str)
                {
                    case "L1": range = GNSSRange.L1; break;
                    case "l1": range = GNSSRange.L1; break;
                    case "1": range = GNSSRange.L1; break;
                    case "L2": range = GNSSRange.L2; break;
                    case "l2": range = GNSSRange.L2; break;
                    case "2": range = GNSSRange.L2; break;
                    case "L5": range = GNSSRange.L5; break;
                    case "l5": range = GNSSRange.L5; break;
                    case "5": range = GNSSRange.L5; break;
                    case "exit": Console.ForegroundColor = ConsoleColor.White; return;
                    case "Exit": Console.ForegroundColor = ConsoleColor.White; return;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Проверьте правильность ввода диапазона");
                        continue;
                }

                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Введите частоту Допплера (от -150000 до 150000 Гц)");

                while (true)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    str = Console.ReadLine();
                    if ((str == "exit") | (str == "Exit"))
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        return;
                    }

                    try
                    {
                        fd = Convert.ToInt32(str);
                        break;
                    }
                    catch
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Проверьте правильность ввода частоты");
                        continue;
                    }
                }

                RegistersCalc(fd, range, out r0, out r1, out r2, out rD);

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Команда синтезатору:");
                Console.WriteLine("SetAdf5 1 0x{0:X6} 0x{1:X6} 0x{2:X6} 0x{3:X6} 0x{4:X6}", r0, r1, r2, R10, rD);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine();
                //Проверка-------------------------------------------------------------------
                //При вызоыве RegistersCalc(0, GNSSRange.L1, out r0, out r1, out r2, out rD) должны вернуться следующие Регистры:
                //R0: 302510, r1: F53F7C1, r2: 1D007D2, rD: D
                //
                //При вызоыве RegistersCalc(130500, GNSSRange.L2, out r0, out r1, out r2, out rD) должны вернуться следующие Регистры:
                //R0: 302740, r1: C0C7E21, r2: 4F82712, rD: D
                //
                //При вызоыве RegistersCalc(0, GNSSRange.L2, out r0, out r1, out r2, out rD) должны вернуться следующие Регистры:
                //R0: 302740, r1: BD70A31, r2: 540192, rD: D
            }
        }

        /// <summary>
        ///  Расчет значения регистров
        /// <param name="fd">Частота Допплра, Гц (изменяется от -150000 до 150000 с шагом 500)</param>
        /// <param name="range">Диапазон</param>
        static void RegistersCalc(int fd, GNSSRange range, out int r0, out int r1, out int r2, out int rD)
        {
            //Выбор частоты гетеродина
            double fg = 0;
            switch (range)
            {
                case GNSSRange.L1: fg = FG_L1; break;
                case GNSSRange.L2: fg = FG_L2; break;
                case GNSSRange.L5: fg = FG_L5; break;
            }
            //Отношение частоты гетеродина к частоте фазового детектора
            double n = (fg + (fd / 1000000d)) / FD;
            //целая часть - пишется в регистр 0.
            int iNT = (int)n;
            //номер регистра
            int registerNum = 0x000000;
            //значение регистра 0
            r0 = (iNT << 4) | 0x300000 | registerNum;
            //дробная часть n
            double remain1 = n - iNT;
            //дробная часть смещенная
            double frac = remain1 * Math.Pow(2, 24);
            //дробная часть увеличенная, переведенная в целое
            int frac1 = (int)frac;
            registerNum = 0x000001;
            //значение регистра 1
            r1 = frac1 << 4 | registerNum;

            //дробная часть, оставшаяся от frac после отделения frac1
            double remain2 = Math.Round(frac-frac1,5);
            int mod2 = (int)Math.Pow(FD, 7) / STEP;
            int frac2 = (int)(remain2 * mod2);
            
            //Поиск наибольшего общего делителя
            int nod = GetNOD(frac2,mod2);
            //Сокращаем
            frac2 /= nod;
            mod2 /= nod;

            //стираем ненужниые биты
            int lSB_Frac = frac2 & 0b00000000000000000011111111111111;
            int lSB_Mod = mod2 & 0b00000000000000000011111111111111;

            registerNum = 0x000002;
            //значение регистра 2
            r2 = ((lSB_Frac << 18) | lSB_Mod << 4) | registerNum;

            //стираем ненужниые биты
            int mSB_Fraq = frac2 & 0b00001111111111111100000000000000;
            int mSB_Mod = mod2 & 0b00001111111111111100000000000000;

            registerNum = 0x00000D;
            //значение регистра D
            rD = ((mSB_Fraq << 4) | (mSB_Mod >> 10)) | registerNum;
        }
       
        /// <summary> Поиск наибольшего общего делителя </summary>
        static int GetNOD(int a, int b)
        {
            while (b != 0)
            {
                var temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }
    }
}
